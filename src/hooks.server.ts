import type { Handle } from '@sveltejs/kit';

export const handle = (async ({ event, resolve }) => {
	if (event.url.searchParams.has('/delete')) {
		event.request = new Request(event.request, { method: 'DELETE' });
	} else if (event.url.searchParams.has('/patch')) {
		event.request = new Request(event.request, { method: 'PATCH' });
	}

	const response = await resolve(event);
	return response;
}) satisfies Handle;
