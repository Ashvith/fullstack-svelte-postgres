import { json } from '@sveltejs/kit';
import type { RequestHandler } from '@sveltejs/kit';
import { api } from '../../api';

export const DELETE = (async ({ request, params }) => {
	const uid = params.uid;
	if (typeof uid === 'string') {
		return api(request, uid);
	}
	return json(null, { headers: { location: '' } });
}) satisfies RequestHandler;

export const PATCH = (async ({ request, params }) => {
	const formData = await request.formData();
	const text = formData.get('text');
	const done = formData.get('done');
	if (typeof text === 'string' || typeof done === 'string') {
		const todoPatch = {
			text: text || undefined,
			done: !!done
		};
		return api(request, params.uid, todoPatch);
	}
	return json(null, { headers: { location: '' } });
}) satisfies RequestHandler;
