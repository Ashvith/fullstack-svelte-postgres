import { json } from '@sveltejs/kit';
import type { RequestHandler } from './$types';
import { api } from '../api';

export const GET = (async ({ request }) => {
	return api(request);
}) satisfies RequestHandler;

export const POST = (async ({ request }) => {
	const text = await request.formData().then((data) => data.get('text'));
	if (typeof text === 'string') {
		const todo = {
			created_at: new Date(),
			text: text,
			done: false
		};
		return api(request, undefined, todo);
	}
	return json(null, { headers: { location: '' } });
}) satisfies RequestHandler;
