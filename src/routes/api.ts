import { json } from '@sveltejs/kit';
import PrismaClient from '$lib/prisma';

const prisma = new PrismaClient();

export const api = async (request: Request, uid?: string, data?: Record<string, unknown>) => {
	let body = null;
	let status = 500;
	let location = '';
	switch (request.method.toUpperCase()) {
		case 'GET':
			body = await prisma.todo.findMany();
			status = 200;
			break;
		case 'POST':
			if (typeof data !== 'undefined') {
				body = await prisma.todo.create({
					data: {
						created_at: data.created_at as Date,
						done: data.done as boolean,
						text: data.text as string
					}
				});
				status = 201;
			}
			break;
		case 'DELETE':
			if (typeof uid !== 'undefined') {
				body = await prisma.todo.findFirst({ where: { uid: uid } });
				await prisma.todo.delete({ where: { uid: uid } });
			}
			status = 200;
			break;
		case 'PATCH':
			if (typeof data !== 'undefined') {
				body = await prisma.todo.update({
					where: { uid: uid },
					data: { done: data.done as boolean, text: data.text as string }
				});
			}
			status = 200;
			break;
	}
	if (
		request.method.toUpperCase() !== 'GET' &&
		request.headers.get('accept') !== 'application/json'
	) {
		status = 303;
		location = '/';
	}
	return json(body, { status: status, headers: { location: location } });
};
